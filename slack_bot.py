# -*- coding: utf-8 -*-
import re
import urllib.request

from bs4 import BeautifulSoup

from flask import Flask
from slack import WebClient
from slackeventsapi import SlackEventAdapter

from datetime import datetime
import dateutil.parser as dparser
from config import *
#구현되있는 기능
#1-2

#


#병합할때 바꿔주기
#SLACK_TOKEN = 'xoxp-685620021686-687026348832-691800059990-eb521d576f582ca6cdf87d404f7c9c9a'
#SLACK_SIGNING_SECRET = '04a6cc32a974d744ffa09d42a611673b'

flag = 0 #어떤 모드에서 slackAPI에 메시지를 보낼지 정해준다. default는 0으로 인사메시지와 어떻게 사용할지 알려준다.
app = Flask(__name__)
# /listening 으로 슬랙 이벤트를 받습니다.
slack_events_adaptor = SlackEventAdapter(SLACK_SIGNING_SECRET, "/listening", app)
slack_web_client = WebClient(token=SLACK_TOKEN)
main_msg = "\n*사용법:*\n"
main_msg += "1. 직무 별 공고 보기 \n"
main_msg += "2. 특정 기업 공고 보기 \n"
main_msg += "3. 기간 별 공고 보기 \n"
main_msg += "4. 실시간 공채속보 \n\n"

main_msg += "*번호 혹은 사용하고 싶으신 메뉴를 말해주세요.* \n"
total_list = []
today = 0
this_week = 0
def job_calender(): #챗봇 호출했을때 공고의 정보를 미리 담아 놓는 로직
    now = datetime.now()
    total = []
    raw_date = str(now.date())
    date_makers = raw_date.split('-')
    real_date = ""
    for date_maker in date_makers:
        real_date += date_maker
    real_date = int(real_date)
    real_week = 0
    t = []
    # divCalendar > table > tbody > tr:nth-child(3)
    url = "http://job.incruit.com/calendar/calendar.asp"
    source_code = urllib.request.urlopen(url).read()
    soup = BeautifulSoup(source_code, "html.parser")
    message=""
    for divCalendar in soup.find_all("div", id="divCalendar"):
        for tbody in divCalendar.find_all("tbody"):
            for i in range(1, 6):
                ii = i * 2 + 1
                titles = tbody.select("tr:nth-child(" + str(ii) + ")>td")
                for title in titles:
                    texts = str(title).split('\n')
                    number = 0

                    for text in texts:
                        number += 1
                        if 'onmouseover' in text:
                            t = []
                            date_1 = text.split('"showAltLayer(')[1]
                            datelink = date_1.split(');" style="position:realitive;"><a href="')
                            date = datelink[0]
                            realdate = date[1:9]
                            week = date[12:13]
                            if int(realdate) == real_date:
                                real_week = int(week)
                            linkimage = datelink[1].split('" name=')
                            link = linkimage[0]
                            imageaddr = linkimage[1].split('src="')[1]
                            image = imageaddr.split('" width')[0].split('SM_')[1]
                            current = image.split('.gif')[0]
                            if current == 'starting':
                                current = '시작'
                            elif current == 'closing':
                                current = '마감'
                            elif current == 'interview':
                                current = '면접'
                            elif current == 'notes':
                                current = '필기'
                            elif current == 'personalitytest':
                                current = '인적성'
                            elif current == 'announcement':
                                current = '서류발표'
                            elif current == 'interviewannouncement':
                                current = '면접발표'
                            elif current == 'notesannouncement':
                                current = '필기발표'
                            elif current == 'personalityannouncement':
                                current = '인적성발표'
                            elif current == 'lastannouncement':
                                current = '최종발표'
                            namedummy = imageaddr.split('/> ')[1]
                            name = namedummy.split('</a>')[0]
                            t.append(name.strip('\'').strip('[').strip(']'))  # 0
                            t.append(int(realdate))  # 1
                            t.append(int(week))  # 2
                            t.append(link)  # 3
                            t.append(current)  # 4
                        elif 'div id' in text:
                            termjob = text.split('" jobtitle="')
                            term = termjob[0].split('invitedt="')[1]
                            job = termjob[1].split('" style')[0]
                            start_date = 0
                            end_date = 99999999
                            if (t[-1] == '시작') or (t[-1] == '마감'):
                                start_term = term.split('  ~ ')[0]
                                end_terms = term.split(' ~ ')[1]
                                end_term = end_terms.split('  [')[0]
                                start_dates = re.search(r'\d{4}년 \d{2}월 \d{2}일', start_term).group(0)
                                end_dates = re.search(r'\d{4}년 \d{2}월 \d{2}일', end_term).group(0)
                                start_dates = start_dates.replace('년 ', '')
                                start_dates = start_dates.replace('월 ', '')
                                start_dates = start_dates.strip('일')
                                start_date = int(start_dates)
                                end_dates = end_dates.replace('년 ', '')
                                end_dates = end_dates.replace('월 ', '')
                                end_dates = end_dates.strip('일')
                                end_date = int(end_dates)
                            else:
                                end_date = t[1]
                            t.append(term)  # 5
                            t.append(job)  # 6
                            t.append(start_date)  # 7
                            t.append(end_date)  # 8
                            message+=t[4]
                            message+=' '
                            message+=t[0]
                            message+=' '
                            message+=t[5]
                            message+=' '
                            message+=t[6]
                            message+='\n'
                            total.append(t)
    if len(message)==0:
        message="*죄송합니다. 찾으시는 결과가 없습니다. 다른 명령어를 입력해 주세요.*\n"
    return total, real_date, real_week

def _chat_home(): #home 챗봇을 켰을때 바로 나온다
    global flag
    flag = 10
    global main_msg
    message = main_msg
    return message


def set_flag(text): # 옵션창에서 flag 조절해주는 함수
    text2 = text.replace(" ", "").lower()
    global flag
    if ("직무" in text2) or ("1" in text2):
        flag = 1
        msg = "*직무 별 공고 보기 입니다.*\n"
        msg += '"생산, 정비, 기능, 노무" // "전문직, 법률, 인문사회, 임원" // "경영, 인사, 총무, 사무"\n'
        msg += '"무역, 영업, 판매, 매장관리" // "교육, 교사, 강사, 교직원" // "전자. 기계, 기술, 화학, 연구개발"\n'
        msg += '"인터넷, IT, 통신, 모바일, 게임" // "재무, 회계, 경리" // "서비스, 여행, 숙박, 음식, 미용"\n'
        msg += '"고객상담, TM" // "건설, 건축, 토목, 환경" // "의료, 간호, 보건, 복지"\n'
        msg += '"마케팅, 광고, 홍보, 조사" // "디자인" // "유통, 무류, 운송, 운전"\n'
        msg += '"금융, 보험, 증권"  // "미디어, 문화, 스포츠"\n\n'
        msg += '*원하시는 직무를 입력해주세요*\n'

    elif ("기업" in text2) or ("2" in text2):
        flag = 2
        msg = "기업 별 공고 보기 입니다.\n"
        msg += "<@봇이름> 기업명 옵션 요청 으로 말해주세요. 정확히 말해주셔야 검색이 됩니다. ㅜㅜ 기업명은 띄어쓰기를 하지 말아주세요.\n"
        msg +="예1) 시작 공고 볼 때-> 기업명 <시작, 언제 시작, 언제시작> + 자유문구"+"\n"+"예2) 마감 공고 볼 때-> 기업명 <마감, 언제끝, 언제 끝> + 자유문구\n"+"예3) 면접 공고 볼 때-> 기업명 <면접, 면접 언제, 면접언제> + 자유문구\n"

    elif ("기간" in text2) or ("3" in text2):
        flag = 3
        msg = "기간 별 공고 보기 입니다.\n"
        msg += "예1) 기간 기반으로 볼 때 -> <이번주, 이번 주> <시작,마감,면접,필기,인적성,면접발표,필기발표,인적성발표,최종발표> + 자유문구\n"
        msg += "예2) 날짜 기반으로 볼 때 -> <yyyy년 mm월 dd일,yyyy년mm월dd일,mm월dd일,dd일,yyyy-mm-dd,yyyy/mm/dd,yy/mm/dd 등> <시작,마감,면접,필기,인적성,면접발표,필기발표,인적성발표,최종발표> + 자유문구\n"


    elif ('4' in text2) or '실시간' in text2 or '실시간 공채' in text2 or '지금 공채' in text2 or '지금' in text2 or '지금 나온' in text2 or '지금나온' in text2 or '속보' in text2 or '채용 속보' in text2 or '채용속보' in text2:
        flag = 4
        curdate=datetime.now()
        curdate=str(curdate)

        msg = "*오늘은 "+curdate+" 입니다.*\n"
        msg += "*실시간 공채 속보를 보여드릴까요?*  \n"

    else:
        msg = "*다시 입력해주세요!*\n"

    return msg
def live_search(text): #실시간 채용 속보   여긴 y or n가 들어감
    message = ""
    text2 = text.replace(" ", "").lower()
    if '응' in text2 or '예' in text2 or '그래' in text2 or 'yes' in text2 or 'ㅇ' in text2 or 'd' in text2 or '네' in text2:
        url = "https://www.incruit.com/"
        source_code = urllib.request.urlopen(url).read()
        soup = BeautifulSoup(source_code, "html.parser")

        # content > div.mainColsWrap > div.mainCntCols > div.mainBtWrap > div.main-liveSearch > ul > li.sider_rank_1 > div > a
        for main_liveSearch in soup.find_all("div", class_="main-liveSearch"):
            for side_ranks in main_liveSearch.find_all("ul", class_="sider_rank_list"):
                for side_rank in side_ranks.find_all("a"):
                    message += str(side_rank.get_text())
                    side_rank=str(side_rank)
                    link_front=side_rank.split('href="')[1]
                    link =link_front.split('" title')[0]
                    message +=' '+link+' '
                    message += "\n\n"
        print(message)
        message += '*한번 더 보여드릴까요?*\n'
    else:
        global flag
        flag = 10
        message = "*궁금한거 더 있나요?*\n"
        global main_msg
        message += main_msg
    return message

def showcompanylist(total,real_date,real_week,order):
    message=""
    if "아니" in order or "노노" in order or "됐어" in order or "뒤로" in order or "그만" in order:
        global flag
        flag = 10
        message = "*궁금한거 더 있나요?*\n"
        global main_msg
        message += main_msg
        return message

    cccc= order.split(' ')
    print(cccc)
    company = cccc[1]
#    options = order.split(' ')[1:]
    category = ""
    print("show함수")
    print(order)
    if '시작' in order or '언제 시작' in order or '언제시작' in order or '시작언제' in order or '시작 언제' in order:
        # 시작 공고 탐색 : 기업명 <시작, 언제 시작, 언제시작> + 자유문구    starting
        category = '시작'
    elif '마감' in order or '언제끝' in order or '언제 끝' in order or '끝언제' in order or '끝 언제' in order:
        # 마감 공고 탐색 : 기업명 <마감, 언제끝, 언제 끝> + 자유문구        closing
        category = '마감'
    elif '면접' in order or '면접 언제' in order or '면접언제' in order or '언제 면접' in order or '언제면접' in order:
        # 면접 공고 탐색 : 기업명 <면접, 면접 언제, 면접언제> + 자유문구    interview
        category = '면접'
    elif '필기' in order or '필기 언제' in order or '필기언제' in order or '언제필기' in order or '필기 언제' in order:
        # 필기 공고 탐색 : 기업명 <필기, 필기 언제, 필기언제> + 자유문구    notes
        category = '필기'
    elif '인적성' in order or '인적성 언제' in order or '인적성언제' in order or '언제인적성' in order or '언제 인적성' in order:
        # 인적성 공고 탐색 : 기업명 <인적성, 인적성 언제, 인적성언제> + 자유문구    personalitytest
        category = '인적성'
    elif '서류' in order or '서류발표' in order or '서류 발표' in order:
        # 서류 발표 탐색 : 기업명 <서류발표, 서류 발표, 서류> + 자유문구            announcement
        category = '서류발표'
    elif '면접발표' in order or '면접 발표' in order:
        # 면접 발표 탐색: 기업명 <면접발표, 면접발표 언제, 면접발표언제, 면접 발표, 면접 발표 언제, 면접 발표언제> + 자유문구 interviewannouncement
        category = '면접발표'
    elif '필기발표' in order or '필기 발표' in order:
        # 필기 발표 탐색: 기업명 <필기발표, 필기 발표, 필기발표 언제, 필기 발표언제, 필기발표언제, 필기 발표 언제> + 자유문구 notesannouncement
        category = '필기발표'
    elif '인적성발표' in order or '인적성 발표' in order:
        # 인적성 발표 탐색: 기업명 <인적성발표, 인적성발표 언제, 인적성발표언제, 인적성 발표, 인적성 발표 언제, 인적성 발표언제> + 자유문구  personalityannouncement
        category = '인적성발표'
    elif '최종발표' in order or '최종 발표' in order:
        # 최종 발표 탐색: 기업명 <최종발표, 최종발표 언제, 최종발표언제, 최종 발표, 최종 발표 언제, 최종 발표언제> + 자유문구         lastannouncement
        category = '최종발표'
    else:
        category = "all"
    print(category)
    print("컴패니 출력하렉요")
    print(company)

    if category == 'all':
        for t in total:
            if t[0]==company:
                message+=t[4]+t[0]+t[5]+t[6]+"\n"
    else:
        for t in total:
            if t[4] == category and t[0].lower() == company.lower():
                if category == '시작' or category == '마감':
                    if real_date - t[7] >= 0 and t[8] - real_date >= 0:
                        message+= "<"+t[3]+"|" + t[0]+">"
                        message+=" 전형 진행 중입니다!\n\n"
                        message+=t[5]+'\n'+t[6]
                    if real_date - t[7] == 0:
                        message+="오늘 시작!! 아직 여유로워요!\n\n "
                        message+="D-"
                        message+=str(t[8] - real_date)
                        message+=' '+t[5]+'\n'+t[6]+'\n'
                    if t[8] - real_date == 0:
                        message+="오늘 마감!! 제출 시간에 유의하세요!\n\n "
                        message+="D-Day\n"
                    if real_date - t[7] < 0:
                        message+=t[0]
                        message+=" 아직 시작 안했어요. 시작 일자: "
                        message+=str(t[7])
                        message+=" D-"
                        message+=str(t[7]-real_date)
                        message+=" 후 시작!"+t[6]+"\n"
                    if t[8] - real_date < 0:
                        message+=t[0]
                        message+=" 이미 끝났습니다. 마감 일자:"
                        message+=str(t[8])
                        message+="\n"
                elif category == '면접':
                    if t[8] - real_date > 0:
                        message+=t[0]
                        message+=" 면접까지 "
                        message+="D-"
                        message+=str(t[8]-real_date)
                        message+=" 일 남았습니다.\n"
                    elif t[8] - real_date == 0:
                        message+="D-day"+"<"+t[3]+"|" + t[0]+">"+" 면접 당일입니다. 면접 잘 보세요!\n"
                    else:
                        message+="면접 일정 종료되었습니다.\n"
                elif category == '필기':
                    if t[8] - real_date > 0:
                        message+="<"+t[3]+"|" + t[0]+">"+" 필기시험까지"+"D-"+str(t[8] - real_date)+" 일 남았습니다.\n"
                    elif t[8] - real_date == 0:
                        message+="D-Day"+ "<"+t[3]+"|" + t[0]+">"+ " 필기시험 당일입니다. 시험 잘 보세요!\n"
                    else:
                        message+="시험 일정 종료되었습니다.\n"
                elif category == '인적성':
                    if t[8] - real_date > 0:
                        message+="<"+t[3]+"|" + t[0]+">"+" 인적성 시험까지 "+"D-"+str(t[8] - real_date)+" 일 남았습니다.\n"
                    elif t[8] - real_date == 0:
                        message+="D-day"+"<"+t[3]+"|" + t[0]+">"+" 인적성 시험 당일입니다. 시험 잘 보세요!\n"
                    else:
                        message+="시험 일정 종료되었습니다.\n"
                elif category == '서류발표':
                    if t[8] - real_date > 0:
                        message += t[0], " 서류발표까지 "+"D-"+str(t[8]-real_date)+" 일 남았습니다.\n"
                    elif t[8] - real_date == 0:
                        message+="D-Day "+"<"+t[3]+"|" + t[0]+">"+" 서류발표 당일입니다. 좋은 결과 있길\n"
                    else:
                        message+="이미 발표했습니다.\n"
                elif category == '면접발표':
                    if t[8] - real_date > 0:
                        message+=t[0]+" 면접 발표까지 "+"D-"+str(t[8]-real_date)+ " 일 남았습니다.\n"
                    elif t[8] - real_date == 0:
                        message+="D-Day "+"<"+t[3]+"|" + t[0]+">"+" 면접발표 당일입니다. 좋은 결과 있길\n"
                    else:
                        message+="이미 발표 했습니다.\n"
                elif category == '필기발표':
                    if t[8] - real_date > 0:
                        message+=t[0]+" 필기발표까지"+" D-"+str(t[8] - real_date)+" 일 남았습니다.\n"
                    elif t[8] - real_date == 0:
                        message+="D-Day "+ "<"+t[3]+"|" + t[0]+">"+ " 필기발표 당일입니다. 좋은 결과 있길\n"
                    else:
                        message += "이미 발표 했습니다.\n"
                elif category == '인적성발표':
                    if t[8] - real_date > 0:
                        message += t[0] + " 인적성발표까지" + " D-" + str(t[8] - real_date) + " 일 남았습니다.\n"
                    elif t[8] - real_date == 0:
                        message += "D-Day " + "<"+t[3]+"|" + t[0]+">" + " 인적성발표 당일입니다. 좋은 결과 있길\n"
                    else:
                        message += "이미 발표 했습니다.\n"
                elif category == '최종발표':
                    if t[8] - real_date > 0:
                        message += t[0] + " 최종발표까지" + " D-" + str(t[8] - real_date) + " 일 남았습니다.\n"
                    elif t[8] - real_date == 0:
                        message += "D-Day " + "<"+t[3]+"|" + t[0]+">" + " 최종발표 당일입니다. 좋은 결과 있길\n"
                    else:
                        message += "이미 발표 했습니다.\n"
        print(message)

    if len(message)==0:
            message="*죄송합니다. 찾으시는 결과가 없습니다. 다른 명령어를 입력해 주세요.*\n"

    message += "\n\n *다른거 더 찾으시는거 있나요??* "
    return message

def showtermlist(total,real_date,real_week,order): #이건 기간별 조회
    message = ""
    if "아니" in order or "노노" in order or "됐어" in order or "뒤로" in order or "그만" in order:
        global flag
        flag = 10
        message = "*궁금한거 더 있나요?*\n"
        global main_msg
        message += main_msg
        return message

    category = ""
    orderlist=order.split(' ')[1:]
    order=""
    for orders in orderlist:
        order+=str(orders)
    print(order)
    order=str(order)
    print(type(order))
    print(order)
    if '시작' in order or '언제 시작' in order or '언제시작' in order or '시작언제' in order or '시작 언제' in order:
        # 시작 공고 탐색 : 기업명 <시작, 언제 시작, 언제시작> + 자유문구    starting
        category = '시작'
    elif '마감' in order or '언제끝' in order or '언제 끝' in order or '끝언제' in order or '끝 언제' in order:
        # 마감 공고 탐색 : 기업명 <마감, 언제끝, 언제 끝> + 자유문구        closing
        category = '마감'
    elif '면접' in order or '면접 언제' in order or '면접언제' in order or '언제 면접' in order or '언제면접' in order:
        # 면접 공고 탐색 : 기업명 <면접, 면접 언제, 면접언제> + 자유문구    interview
        category = '면접'
    elif '필기' in order or '필기 언제' in order or '필기언제' in order or '언제필기' in order or '필기 언제' in order:
        # 필기 공고 탐색 : 기업명 <필기, 필기 언제, 필기언제> + 자유문구    notes
        category = '필기'
    elif '인적성' in order or '인적성 언제' in order or '인적성언제' in order or '언제인적성' in order or '언제 인적성' in order:
        # 인적성 공고 탐색 : 기업명 <인적성, 인적성 언제, 인적성언제> + 자유문구    personalitytest
        category = '인적성'
    elif '서류' in order or '서류발표' in order or '서류 발표' in order:
        # 서류 발표 탐색 : 기업명 <서류발표, 서류 발표, 서류> + 자유문구            announcement
        category = '서류발표'
    elif '면접발표' in order or '면접 발표' in order:
        # 면접 발표 탐색: 기업명 <면접발표, 면접발표 언제, 면접발표언제, 면접 발표, 면접 발표 언제, 면접 발표언제> + 자유문구 interviewannouncement
        category = '면접발표'
    elif '필기발표' in order or '필기 발표' in order:
        # 필기 발표 탐색: 기업명 <필기발표, 필기 발표, 필기발표 언제, 필기 발표언제, 필기발표언제, 필기 발표 언제> + 자유문구 notesannouncement
        category = '필기발표'
    elif '인적성발표' in order or '인적성 발표' in order:
        # 인적성 발표 탐색: 기업명 <인적성발표, 인적성발표 언제, 인적성발표언제, 인적성 발표, 인적성 발표 언제, 인적성 발표언제> + 자유문구  personalityannouncement
        category = '인적성발표'
    elif '최종발표' in order or '최종 발표' in order:
        # 최종 발표 탐색: 기업명 <최종발표, 최종발표 언제, 최종발표언제, 최종 발표, 최종 발표 언제, 최종 발표언제> + 자유문구         lastannouncement
        category = '최종발표'
    else:
        category = "all"
    if order.startswith('이번') or order.startswith('이번 주'):
        if (category == 'all'):
            print("이번 주 모든 공고입니다.")
            message+="*이번 주 모든 공고입니다.*\n"  #"<https://ssafy.elice.io|엘리스>는 정말 최고야!"
        for t in total:
            if t[4] == category and t[2] == real_week:
                if category == '시작' or category == '마감':
                    if t[8] - real_date > 0 and real_date - t[7] >= 0:
                        message+= "<"+t[3]+"|"+ t[0]+"> " "아직 진행 중!! "+"D-"+str(t[8] - real_date)+ "뒤 마감!!\n"+"\n"
                    if t[8] - real_date == 0:
                        message+="D-DAY "+  "<"+t[3]+"|"+ t[0]+"> "+ " 오늘 마감!! \n"+"\n"
                    if real_date - t[7] < 0:
                        message+="<"+t[3]+"|"+ t[0]+"> "+" 아직 시작 하지 않은 공고\n"+"\n"
                    if real_date - t[8] > 0:
                        message+=t[0]+ " 이미 끝난 공고\n"+"\n"
            if category == 'all' and t[2] == real_week:
                if t[8] - real_date < 0:
                    message+="\n이미 끝난 공고 : "+t[0]+' '+t[5]+' '+t[6]+'\n'+"\n"
                if t[7] - real_date > 0:
                    message+="\n시작 예정인 공고 : "
                if t[8] - real_date >= 0 and real_date - t[7] >= 0:
                    message+="\n진행 중인 공고 : "
                    message+="<"+t[3]+"|"+ t[0]+"> "+' '+t[5]+' '+t[6]+"\n"+"\n"
            message+"\n\n"
    if order.startswith('0') or order.startswith('1') or order.startswith('2') or order.startswith('3') or order.startswith('4') or order.startswith('5') or order.startswith('6') or order.startswith('7') or order.startswith('8') or order.startswith('9'):
        order_date = dparser.parse(order, fuzzy=True)
        order_date = str(order_date)
        order_date = order_date.split(' ')[0]
        order_date = order_date.replace('-', '')
        print(order_date[0:8])
        order_date = int(order_date[0:8])
        for t in total:
            if t[7] <= order_date and order_date <= t[8]:
                edate = str(t[8])
                end_y = edate[0:4]
                end_m = edate[4:6]
                end_d = edate[6:]
                endt = str(end_y) + '년 ' + str(end_m) + '월 ' + str(end_d) + '일'
                if order_date == t[8]:
                    message+=endt+"에 마감되는 공고: \t "
                    message += "<"+t[3]+"|"+ t[0]+"> " + ' ' + t[5] + ' ' + t[6] + "\n\n"
                else:
                    message += endt + "까지 진행되는 공고: \t"
                    message += "<"+t[3]+"|"+ t[0]+"> " + ' ' + t[5] + ' ' + t[6] + "\n\n"
    if len(message)==0:
        message="*죄송합니다. 찾으시는 결과가 없습니다. 다른 명령어를 입력해 주세요.*\n"
    message += "\n\n *다른거 더 찾으시는거 있나요??* "
    return message
# 크롤링 함수 구현하기
def _crawl_gonggo_chart(text):
    text2 = text.replace(" ", "").lower()
    global flag
    print(text2)
    main_url = "http://job.incruit.com/jobdb_list/searchjob.asp?today=y"
    url = ""
    flag_from_gonggo_chart=0
    if ("it" in text2) or ("통신" in text2) or ("게임" in text2) or ("모바일" in text2) or ("인터넷" in text2):
        url += main_url
        url += "&occ1=150"
        flag_from_gonggo_chart = 1

    if ("생산" in text2) or ("정비" in text2) or ("기능" in text2) or ("노무" in text2):
        url += main_url
        url += "&occ1=170"
        flag_from_gonggo_chart = 1

    if ("전문직" in text2) or ("법률" in text2) or ("인문사회" in text2) or ("임원" in text2):
        url += main_url
        url += "&occ1=130"
        flag_from_gonggo_chart = 1

    if ("경영" in text2) or ("인사" in text2) or ("총무" in text2) or ("사무" in text2):
        url += main_url
        url += "&occ1=100"
        flag_from_gonggo_chart = 1

    if ("무역" in text2) or ("영역" in text2) or ("판매"in text2) or ("매장" in text2):
        url += main_url
        url += "&occ1=160"
        flag_from_gonggo_chart = 1

    if ("교육" in text2) or ("교사" in text2) or ("강사" in text2) or ("교직원" in text2):
        url += main_url
        url += "&occ1=103"
        flag_from_gonggo_chart = 1

    if ("전자" in text2) or ("기계" in text2) or ("기술" in text2) or ("화학" in text2) or ("연구개발" in text2):
        url += main_url
        url += "&occ1=120"
        flag_from_gonggo_chart = 1

    if ("재무"in text2) or ("회계"in text2) or ("경리"in text2) :
        url += main_url
        url += "&occ1=101"
        flag_from_gonggo_chart = 1

    if ("서비스" in text2) or ("여행" in text2) or ("숙박" in text2) or ("음식" in text2) or ("미용" in text2):
        url += main_url
        url += "&occ1=140"
        flag_from_gonggo_chart = 1

    if ("고객상담" in text2) or ("tm" in text2):
        url += main_url
        url += "&occ1=106"
        flag_from_gonggo_chart = 1

    if ("건설" in text2) or ("건축"in text2) or ("토목" in text2) or ("환경" in text2):
        url += main_url
        url += "&occ1=107"
        flag_from_gonggo_chart = 1

    if ("의료" in text2) or ("간호" in text2) or ("복지" in text2) or ("보건" in text2):
        url += main_url
        url += "&occ1=190"
        flag_from_gonggo_chart = 1

    if ("마케팅"in text2) or ("광고" in text2) or ("홍보" in text2):
        url += main_url
        url += "&occ1=102"
        flag_from_gonggo_chart = 1

    if "디자인" in text2:
        url += "&occ1=104"
    if ("유통" in text2) or ("물류" in text2) or ("운송" in text2) or ("운전" in text2):
        url += main_url
        url += "&occ1=110"
        flag_from_gonggo_chart = 1

    if ("금융" in text2) or ("보험" in text2) or ("증권" in text2):
        url += main_url
        url += "&occ1=210"
        flag_from_gonggo_chart = 1

    if ("미디어" in text2) or ("문화" in text2) or ("스포츠" in text2):
        url += main_url
        url += "&occ1=200"
        flag_from_gonggo_chart = 1

    if flag_from_gonggo_chart ==1:
        print(url)
        source_code = urllib.request.urlopen(url).read()
        soup = BeautifulSoup(source_code,"html.parser")

        title = []
        subjects = []
        contents = []
        options = []
        deadlines = []
        links = []
        message = "*<직무 별 공고>*\n\n"
        for x in soup.find_all('tbody'):
            for text in x.find_all('span', class_="accent"):
                contents.append(text.get_text().replace('\n','').replace('\r',''))
            for text in x.find_all('p', class_="details_txts firstChild"):
                subjects.append(text.get_text().replace('\n','').replace('\r',''))
            for text in x.find_all('span', class_="links"): #기업명 가져오기
                title.append(text.get_text().replace('\n','').replace('\r','').replace('관심기업',''))
                links.append(text.find("a")['href'])

            for y in x.find_all('div', class_="subjects termArea"):
                for text in y.find_all('p', class_="details_txts firstChild"):
                    text2=text.get_text().split('\n')
                    # text.get_text().replace('\n', '').replace('\r', '')
                    options.append(text2[2].replace('\r', ''))
                    # print(text2)
                    # options.append(text.get_text().replace('\n', '').replace('\r', ''))

            for text in x.find_all('div', class_="ddays"):
                deadlines.append(text.get_text().replace('\n', '').replace('\r', '').replace('바로지원',''))
        print('\n')
        print(links)

        for i in range(10): #우선은 10개만...
            message += "="*50
            message += "\n"
            message += "회사명:"
            message += "\t"
            message += "<"
            message += str(links[i])
            message += "|"
            message += str(title[i])
            message += ">"
            message += "\n"
            message += "모집분야:"
            message += "\t"
            message += str(subjects[i])     #"<https://ssafy.elice.io|엘리스>는 정말 최고야!"
            message += "\n"
            message += "상세 내용:"
            message += "\t"
            message += str(contents[i])
            message += "\n"
            message += "근무 위치:"
            message += "\t"
            message += str(options[i])
            message += "\n"
            message += "마감일:"
            message += "\t"
            message += str(deadlines[i])
            message += "\n"
            message += "\n"
        message += "*더 찾으시는 직무가 있나요?*"
        return message
    elif ("아니" in text2) or ("no" in text2):

        flag = 10
        message = "*궁금한거 더 있나요?*\n"
        global main_msg
        message += main_msg
        return message
    else:
        return "*잘못된 입력입니다! ^^* "

# 챗봇이 멘션을 받았을 경우
@slack_events_adaptor.on("app_mention")
def app_mentioned(event_data):
    channel = event_data["event"]["channel"]
    text = event_data["event"]["text"]
    final_message = "*안녕하세요 5team 챗봇입니다!*\n"
    global flag
    global total_list
    global today
    global this_week
    if flag == 0 or text == "0":
        tmp_message =_chat_home()
        final_message +=tmp_message
        total_list, today, this_week = job_calender()


    elif '사용법' in text:
        final_message =_chat_home()
    elif flag == 10:
        final_message = set_flag(text)
    elif flag == 1:
        message_from_1 = _crawl_gonggo_chart(text)
        final_message = message_from_1
    elif flag == 2:
        message_from_2 = showcompanylist(total_list,today,this_week,text)
        final_message = message_from_2
    elif flag == 3:
        message_from_3 = showtermlist(total_list,today,this_week,text)
        final_message = message_from_3
    elif flag == 4 :
        message_from_4 = live_search(text)  #얘는 응 아니오만 받는다
        final_message = message_from_4

    print(flag)

    slack_web_client.chat_postMessage(
        channel=channel,
        text=final_message
    )


# / 로 접속하면 서버가 준비되었다고 알려줍니다.
@app.route("/", methods=["GET"])
def index():
    return "<h1>공고Server is ready.</h1>"


if __name__ == '__main__':
    app.run('0.0.0.0', port=5004)